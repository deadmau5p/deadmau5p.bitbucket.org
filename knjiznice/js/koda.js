
var baseUrl = 'https://rest.ehrscape.com/rest/v1';
var queryUrl = baseUrl + '/query';

var username = "ois.seminar";
var password = "ois4fri";


/**
 * Generiranje niza za avtorizacijo na podlagi uporabniškega imena in gesla,
 * ki je šifriran v niz oblike Base64
 *
 * @return avtorizacijski niz za dostop do funkcionalnost
 **/
function getAuthorization() {
  return "Basic " + btoa(username + ":" + password);
}


/**
 * Generator podatkov za novega pacienta, ki bo uporabljal aplikacijo. Pri
 * generiranju podatkov je potrebno najprej kreirati novega pacienta z
 * določenimi osebnimi podatki (ime, priimek in datum rojstva) ter za njega
 * shraniti nekaj podatkov o vitalnih znakih.
 * @param stPacienta zaporedna številka pacienta (1, 2 ali 3)
 * @return ehrId generiranega pacienta
 **/
function generirajPodatke(stPacienta) {
  ehrId = "";

  // TODO: Potrebno implementirati

  return ehrId;
}


// TODO: Tukaj implementirate funkcionalnost, ki jo podpira vaša aplikacija
function prikaziPodatke() {
	var bol = $("#preberiBolnika").val();
	

	if (!bol|| bol.trim().length == 0 ) {
		
	} else {
		$.ajax({
			url: baseUrl + "/demographics/ehr/" + bol + "/party",
	    	type: 'GET',
	    	headers: {
          "Authorization": getAuthorization()
        },
	    	success: function (data) {
  				var party = data.party;
  				
  				
  				
  					$.ajax({
    			    url: baseUrl + "/view/" + ehrId + "/" + "weight",
    			    type: 'GET',
    			    headers: {
                "Authorization": getAuthorization()
              },
    			    success: function (res) {
    			    	if (res.length > 0) {
    				    	var results = "<table class='table table-striped " +
                    "table-hover'><tr><th>Datum in ura</th>" +
                    "<th class='text-right'>Telesna teža</th></tr>";
  				        for (var i in res) {
    		            results += "<tr><td>" + res[i].time +
                      "</td><td class='text-right'>" + res[i].weight + " " 	+
                      res[i].unit + "</td></tr>";
  				        }
  				        results += "</table>";
  				        $("#rezultati").append(results);
    			    	}
    			    }
    			    
  					});
  				}
	    	});
	    	
	}
}

if(document.getElementById(identiteta-bolnika).value == "Luka Norec"){
  var luka = {
    "stPacienta" : "1",
    "Ime" : "Luka",
    "Priimek" : "Norec",
    "Teže" : ["85", "84", "83", "86", "87"],
    "Višina" : "1.80",
    "Leta" : "35",
    
    
  }
  
  
}else if(document.getElementById(identiteta-bolnika).value == "Boštjan Suhadolec"){
  var Boštjan = {
    "stPacienta" : "2",
    "Ime" : "Boštjan",
    "Priimek" : "Suhadolec",
    "Teže" : ["65", "67", "70", "68", "67"],
    "Višina" : "1.850",
    "Leta" : "25",
    
    
  }
}else if(document.getElementById(identiteta-bolnika).value == "Primož Jedec"){
  var Primož = {
    "stPacienta" : "3",
    "Ime" : "Primož",
    "Priimek" : "Jedec",
    "Teže" : ["100", "105", "103", "104", "99"],
    "Višina" : "1.70",
    "Leta" : "55",
    
    
  }
}  